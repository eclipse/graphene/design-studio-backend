/*-
 * ===============LICENSE_START=======================================================
 * Acumos
 * ===================================================================================
 * Copyright (C) 2017 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
 * ===================================================================================
 * This Acumos software file is distributed by AT&T and Tech Mahindra
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * This file is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ===============LICENSE_END=========================================================
 */

package org.acumos.designstudio.ce.vo.blueprint;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

public class BluePrint implements Serializable {

	private static final long serialVersionUID = 1L;

	private String type="pipeline-topology/v2";
	private String name;
	private String version;
	private String creation_date= Timestamp.from(Instant.now()).toString();
	private String pipeline_id;
	private List<Node> nodes;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCreation_date() {
		return creation_date;
	}

	public String getType() {
		return type;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public String getPipeline_id() { return pipeline_id; }

	public void setPipeline_id(String pipeline_id) { this.pipeline_id = pipeline_id; }
}
